$(() => {
    cubePNG = window['sr-visualizer'].cubeSVG
    cubeSVG = window['sr-visualizer'].cubeSVG
    Face = window['sr-visualizer'].Face

	let faceColors = {
		u: 'White',
		f: 'Green',
		r: 'Red',
		b: 'Blue',
		l: 'Orange',
		d: 'Yellow'
	}

    function stateToFacelets(state) {
        return applyChange(
            state, [
                //UFRBLD -> URFDLB
                0, 1, 2,
                3, 4, 5,
                6, 7, 8,

                18, 19, 20,
                21, 22, 23,
                24, 25, 26,

                9, 10, 11,
                12, 13, 14,
                15, 16, 17,

                45, 46, 47,
                48, 49, 50,
                51, 52, 53,

                36, 37, 38,
                39, 40, 41,
                42, 43, 44,

                27, 28, 29,
                30, 31, 32,
                33, 34, 35
            ])
    }

    let colors = {
        'u': '#ecf0f1',
        'f': '#8CC152',
        'r': '#E83A3A',
        'b': '#2980b9',
        'l': '#e67e22',
        'd': '#f1c40f',
        'n': '#AAB2BD',
        'a': 'black'
    }

    let colorScheme = {
        [0]: colors.u,
        [1]: colors.r,
        [2]: colors.f,
        [3]: colors.d,
        [4]: colors.l,
        [5]: colors.b
    }

    let faces = 'ufrbld'.split('')
    let stickerElements = []
    for (let n = 0; n < 54; n++) {
        let side = Math.floor(n / 9)
        $(`#s${n}`).append(`
	<div class="form-check form-check-inline" id="s${n}uc">
	<input class="form-check-input" type="checkbox" ${side==0?"checked":""} id="s${n}u">
	<label class="form-check-label" for="s${n}u">White</label>
	</div>
	<div class="form-check form-check-inline" id="s${n}fc">
	<input class="form-check-input" type="checkbox" ${side==1?"checked":""} id="s${n}f">
	<label class="form-check-label" for="s${n}f">Green</label>
	</div>
	<div class="form-check form-check-inline" id="s${n}rc">
	<input class="form-check-input" type="checkbox" ${side==2?"checked":""} id="s${n}r">
	<label class="form-check-label" for="s${n}r">Red</label>
	</div>
	<div class="form-check form-check-inline" id="s${n}bc">
	<input class="form-check-input" type="checkbox" ${side==3?"checked":""} id="s${n}b">
	<label class="form-check-label" for="s${n}b">Blue</label>
	</div>
	<div class="form-check form-check-inline" id="s${n}lc">
	<input class="form-check-input" type="checkbox" ${side==4?"checked":""} id="s${n}l">
	<label class="form-check-label" for="s${n}l">Orange</label>
	</div>
	<div class="form-check form-check-inline" id="s${n}dc">
	<input class="form-check-input" type="checkbox" ${side==5?"checked":""} id="s${n}d">
	<label class="form-check-label" for="s${n}d">Yellow</label>
	</div>`)

        let stickerCheckboxes = []
        for (let face in faces) {
            let el = $(`#s${n}${faces[face]}`)

            stickerCheckboxes.push(el)

        }
        stickerElements.push(stickerCheckboxes)
    }



    function formatInput() {
        return stickerElements.map((sticker, stickerIndex) => {
            return sticker.map((box, boxIndex) => {
                return [box.prop('checked'), boxIndex]
            }).filter(a => a[0]).map(a => a[1])
        })
    }

    window.base64Encode = function() {
        let key = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
        let result = stickerElements.map(sticker => {
            return key[
                sticker.map((box, index) => {
                    let value = (box.prop('checked') ? 1 : 0) * (1 << index)
                    return value
                }).reduce((a, b) => a + b)
            ]
        }).reduce((a, b) => a + b)
        return result
    }

    window.base64Decode = function(data) {
        console.log('importing state')
        let key = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
        let faces = 'ufrbld'.split('')
        for (let i in data) {
            for (let j in faces) {
                let el = $(`#s${i}${faces[j]}`)
                // console.log(`#s${i}${faces[j]}`)
                if ((key.indexOf(data[i]) & (1 << j)) >> j != el[0].checked) {
                    el[0].click()
                    el.hide().show(0)
                }
            }
            $(`#s${i}uc`).parent().click()
        }
        $("#cubestate").hide().show(0)
        $("#cubestate").trigger('click');
        // window.base64Encode();
    }

    let renderingOn = false

    function renderState() {
        let facelets = []

        let input = formatInput()
        stickerElements.forEach((el, i) => {
            $(`#s${i}`).removeClass(`msticker`)

            faces.forEach((face, j) => {
                if (JSON.stringify(input[i]) != JSON.stringify([j])) {
                    $(`#s${i}`).removeClass(`${face}sticker`)
                } else {
                    $(`#s${i}`).addClass(`${face}sticker`)
                    facelets.push(face)
                }
            })
            if (input[i].length > 1) {
                $(`#s${i}`).addClass(`msticker`)
                facelets.push('a')
            }
            if (input[i].length == 0) {
                facelets.push('n')
            }
        })
        statePaste.value = base64Encode()
        if (renderingOn) {
            $('#ufr').html('')
            $('#all').html('')
            $('#bld').html('')
            cubeSVG($('#ufr')[0], {
                stickerColors: stateToFacelets(facelets).map(a => colors[a])
            })
            cubeSVG($('#all')[0], {
                stickerColors: stateToFacelets(facelets).map(a => colors[a]).map(
                    a =>
                    a == colors.n ? 'rgba(0,0,0,0.0)' : a
                ),
                cubeOpacity: 0,
                stickerOpacity: 60
            })
            cubeSVG($('#bld')[0], {
                stickerColors: stateToFacelets(applyChange(applyChange(facelets, turns.x2), turns.yi)).map(a => colors[a])
            })
        }
    }

    renderState()

    cubestate.onclick = function() {
        renderState()

        localStorage.setItem('state', base64Encode(statePaste.value))
    }

    reset.onclick = function() {
        statePaste.value = 'BBBBBBBBBCCCCCCCCCEEEEEEEEEIIIIIIIIIQQQQQQQQQggggggggg'
        base64Decode(statePaste.value)
    }

    load.onclick = function() {
        base64Decode(statePaste.value)
    }


	function solve() {
        const moves = []
        let gen = $('#gen').val().split('')
        let maxDepth = parseInt($('#depth').val())
        console.log(gen)
        for (let i in gen) {
            moves.push(gen[i])
            moves.push(gen[i] + 'i')
            moves.push(gen[i] + '2')
        }
        console.log(moves)

        let input = formatInput()
        let algs = []
        // for (let depth = 0; depth <= maxDepth; depth++) {
            algs = algs.concat(search(input, moves, maxDepth, moves.length, moves.length).solutions)
        // }
        // console.log(algs)
        // algs = algs.map(alg => alg.)
        // console.log(algs)
		// if()


		if(algs.length) {
			$('#output').html(algs.map(a => {
				let newA = [a[0]]
				for (let i = 1; i < a.length; i++) {
					if (a[i] === a[i - 1]) {
						newA.push(newA.pop() + '2')
					} else {
						newA.push(a[i])
					}
				}
				return newA.join(' ')
			}).map(a => a == "" ? "Solved" : a).map(a => `<div class="alg">${a}</div>`).join('').split('i ').join('\' ').split('i<').join('\'<'))

			$.each($('.alg'), (key, value) => {
				let alg = value.innerHTML
				$(value).prepend('<span class="image2"></span>')
				$(value).prepend('<span class="image1"></span>')

				if (alg === 'Solved') {
					alg = ''
				}
				cubeSVG($(value).find('.image1')[0], {
					case: alg,
					colorScheme: colorScheme
				})
				cubeSVG($(value).find('.image2')[0], {
					view: 'plan',
					case: alg,
					colorScheme: colorScheme
				})
			})
		} else {
			$('#output').html('None')
		}

		alert('Done solving.')
	}
	window.solving = false

    solvebutton.onclick = () => {
		if(!solving) {
			console.log('solve start')
			solving = true
			setTimeout(function() {
				solve()
				solving = false
				console.log('solve end')
			})
		} else {
			console.log('TOO MANY CLICKS')
		}
    }

    if (localStorage.getItem('state') && localStorage.getItem('state') != 'null') {
        statePaste.value = localStorage.getItem('state')
        base64Decode(statePaste.value)
    }
    renderingOn = true
    renderState()
})
