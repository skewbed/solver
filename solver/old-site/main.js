async function main() {
	const response = await fetch('algs.txt');
	const text = await response.text();
	console.log(text)
	const lines = text.split('\n').filter(a=>a)

	lines.forEach(line => {
		algs.innerHTML +=
		`<div class="alg">
			<img src="http://cube.crider.co.uk/visualcube.png?view=plan&case=${line.split(' ').join('')}" />
			${line}
		</div>`
	})
}


window.onload = main
