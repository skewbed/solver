// startingPosition = 0,1,2,3,4
// change = 4,3,2,1,0
// result = 4,3,2,1,0
function applyChange(startingPosition, change) {
    let result = []
    for (let i in startingPosition) {
        result.push(startingPosition[change[i]])
    }
    return result
}

let ID = []

for (let i = 0; i < 54; i++) {
    ID.push(i)
}

let U = pos => applyChange(pos, [
    6, 3, 0,
    7, 4, 1,
    8, 5, 2,

    18, 19, 20,
    12, 13, 14,
    15, 16, 17,

    27, 28, 29,
    21, 22, 23,
    24, 25, 26,

    36, 37, 38,
    30, 31, 32,
    33, 34, 35,

    9, 10, 11,
    39, 40, 41,
    42, 43, 44,

    45, 46, 47,
    48, 49, 50,
    51, 52, 53
])

let y = pos => applyChange(pos, [
    6, 3, 0,
    7, 4, 1,
    8, 5, 2,

    18, 19, 20,
    21, 22, 23,
    24, 25, 26,

    27, 28, 29,
    30, 31, 32,
    33, 34, 35,

    36, 37, 38,
    39, 40, 41,
    42, 43, 44,

    9, 10, 11,
    12, 13, 14,
    15, 16, 17,

    47, 50, 53,
    46, 49, 52,
    45, 48, 51
])
let x = pos => applyChange(pos, [
    9, 10, 11,
    12, 13, 14,
    15, 16, 17,

    45, 46, 47,
    48, 49, 50,
    51, 52, 53,

    24, 21, 18,
    25, 22, 19,
    26, 23, 20,

    8, 7, 6,
    5, 4, 3,
    2, 1, 0,

    38, 41, 44,
    37, 40, 43,
    36, 39, 42,

    35, 34, 33,
    32, 31, 30,
    29, 28, 27
])

let Ui = a => U(U(U(a)))
let yi = a => y(y(y(a)))
let xi = a => x(x(x(a)))

let basicTurns = {
    'U': [U],
    'R': [y, x, U, xi, yi],
    'L': [yi, x, U, xi, y],
    'F': [x, U, xi],
    'B': [xi, U, x],
    'D': [x, x, U, x, x],
    'x': [x],
    'y': [y],
    'z': [yi, x, y]
}

let turns = [

]

function draw(state) {
    for (let face = 0; face < 6; face++) {
        for (let row = 0; row < 3; row++) {
            let rowText = ''
            for (let col = 0; col < 3; col++) {
                color = state[face * 9 + row * 3 + col]
                rowText +=
                    'WGRBOY' [Math.floor(color / 9)]
            }
            console.log(rowText)

        }
        console.log()
    }
}

function applyTurnFunctions(funcs) {
    let _funcs = funcs.slice(0)
    let result = ID.slice(0)
    while (_funcs.length) {
        result = _funcs.shift()(result)
    }
    return result
}

for (let i in basicTurns) {
    let turn = basicTurns[i].slice(0)
    turns[i] = applyTurnFunctions(turn)
    turns[i + '2'] = applyTurnFunctions(turn.concat(turn))
    turns[i + 'i'] = applyTurnFunctions(turn.concat(turn.concat(turn)))
}

function applyTurnSequence(pos, seq) {
    let _pos = pos.slice(0)
    let _seq = seq.slice(0)
    while (_seq.length) {
        _pos = applyChange(_pos, turns[_seq.shift()])
    }
    return _pos
}

// draw(applyTurnSequence(ID,['R','Ri','R','y2','Li','y2']))

// sticker possiblities listed in what it can't be

function isSolved(position) {
    for (let i in position) {
        if (position[i].length && position[i].indexOf(Math.floor(i / 9)) == -1) {
            return false
        }
    }
    return true
}

function forceNotSticker(num) {
    return [0, 1, 2, 3, 4, 5].filter(a => a != num)
}

let basicCube = []
for (let i in ID) {
    basicCube.push([Math.floor(i / 9)])
}

let opposites = {
    U: 'D',
    D: 'U',
    F: 'B',
    B: 'F',
    R: 'L',
    L: 'R'
}

// returns [validBranch,solutions]
function search(cube, subset, depth, last, beforeLast) {
	if (isSolved(cube)) {
		// console.log(cube)
		return {
			valid: true,
			solutions: [
				[]
			]
		}
	}
    // console.log('DEPTH', depth, last, beforeLast)
    if (depth == 0) {
        // console.log('SOLVED',isSolved(cube))
        return {
            valid: false,
            solutions: []
        }
    }
    let validResults = []
    for (let i in subset) {
        // if(subset[i].length == 2 && i == last) {
        // 	continue;
        // }
        let useless = false
        // if they start with the same letter
        if (last != subset.length) {
            if (subset[i][0] == subset[last][0]) {
                useless = true
            }
            if (beforeLast != subset.length) {
                if (subset[i][0] == subset[beforeLast][0] &&
                    opposites[subset[i][0]] == subset[last][0]) {
                    useless = true
                }
            }

        }



        if (!useless) {
            let turned = applyChange(cube, turns[subset[i]])
            let result = search(turned, subset, depth - 1, i, last)
            if (result.valid) {
                for (let j in result.solutions) {
                    validResults.push([subset[i]].concat(result.solutions[j]))
                }
            }
        }
    }
    return {
        valid: validResults.length != 0,
        solutions: validResults
    }
}

// console.log(applyChange(basicCube,turns.R))


const L5C = [
    [1],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],

    [],
    [],
    [],
    [1],
    [],
    [],
    [1],
    [],
    [1],

    [],
    [],
    [],
    forceNotSticker(2), [2],
    [2],
    [2],
    [2],
    [2],

    [],
    [],
    [],
    [3],
    [],
    [3],
    [3],
    [],
    [3],

    [],
    [],
    [],
    [4],
    [4],
    [4],
    [4],
    [4],
    [4],

    [5],
    [],
    [5],
    [5],
    [],
    [5],
    [5],
    [],
    [5],

].map(a => a.length ? a : [0, 1, 2, 3, 4, 5])

const LL = [
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [1],
    [1],
    [1],
    [1],
    [1],
    [1],

    [],
    [],
    [],
    [2],
    [2],
    [2],
    [2],
    [2],
    [2],

    [],
    [],
    [],
    [3],
    [3],
    [3],
    [3],
    [3],
    [3],

    [],
    [],
    [],
    [4],
    [4],
    [4],
    [4],
    [4],
    [4],

    [5],
    [5],
    [5],
    [5],
    [5],
    [5],
    [5],
    [5],
    [5]
].map(a => a.length ? a : [0, 1, 2, 3, 4, 5])

// console.log(isSolved(L5C))

// let algs = []
//
// for(let i = 0; i <= 11; i++) {
// 	algs = algs.concat(search(
// 		L5C, ['R', 'Ri', 'F', 'Fi', 'U', 'Ui'],i,100,100
// 	).solutions)
// }
//
// algs = algs
// .map(a => {
// 	while(a[0] == 'U' || a[0] == 'Ui') {
// 		a.shift()
// 	}
// 	while(a[a.length-1] == 'U' || a[a.length-1] == 'Ui') {
// 		a.pop()
// 	}
// 	return a
// }).map(
// 	a => a.join(' ').split('i').join('\'')
// )
//
// algs = [...new Set(algs)]
//
// algs.forEach(a => console.log(a))
//
// //
